# This is a repository to list approved modules.

Each folder will be for a different flast version.

Each file will be for a specific module type.

The current format is a JSON object of the following form:
```
[
...,
{
    "name":"Awesome Module Name",
    "description":"Small description to explain what the module does",
    "URL":"url to the module, https will get enforced",
    "projectURL":"url to the project page (source/issues/info...), https will get enforced"
},
...]
```

Do a merge request if you wish to get your module approved.
